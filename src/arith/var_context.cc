#include <tvm/arith/analyzer.h>
#include <tvm/arith/egg_simpl.h>
#include <tvm/arith/var_context.h>
#include <tvm/tir/expr_functor.h>
#include <tvm/tir/op.h>
#include <tvm/tir/stmt_functor.h>

#include <numeric>

namespace tvm {
namespace arith {

using namespace tir;

TVM_REGISTER_NODE_TYPE(VarContextNode);

inline bool HasDiv(const PrimExpr& expr) {
  bool found = false;
  PostOrderVisit(expr, [&expr, &found](const ObjectRef& node) {
    if (node->IsInstance<DivNode>()) {
      found = true;
    }
  });
  return found;
}

inline std::pair<PrimExpr, PrimExpr> ConservativeDiv(PrimExpr extent, PrimExpr factor,
                                                     bool no_tighten_factor) {
  auto min_factor = no_tighten_factor ? factor : min(extent, factor);
  auto divided = indexdiv(extent + (factor - 1), factor);
  return std::make_pair(min_factor, divided);
}

SizeVar VarContextNode::MakeAndInsertVar(PrimExpr expr, bool is_shorthand) {
  std::string name = (is_shorthand ? "v" : "d") + std::to_string(this->var_to_expr.size());
  return this->var_to_expr.try_push_back(name, expr);
}

std::pair<PrimExpr, PrimExpr> VarContextNode::SymbolicDiv(PrimExpr numer, PrimExpr denom,
                                                          bool no_tighten_factor) {
  auto [begin, end] = this->_div_map.equal_range(numer);
  for (; begin != end; ++begin) {
    PrimExpr simpl = SimplifyExpr(begin->second / denom);
    if (!HasDiv(simpl)) {
      return {denom, simpl};
    }
  }
  PrimExpr simpl = SimplifyExpr(numer / denom);
  if (HasDiv(simpl)) {
    return ConservativeDiv(numer, denom, no_tighten_factor);
  } else {
    return {denom, simpl};
  }
}

PrimExpr VarContextNode::AttemptShorten(PrimExpr expr) {
  if (ExprIsConst(expr) && CountOps(expr) >= 10) {
    expr = this->MakeAndInsertVar(arith::SimplifyExpr(expr), true);
  }
  return expr;
}

std::pair<PrimExpr, PrimExpr> VarContextNode::GetSplitSizes(PrimExpr extent, PrimExpr factor,
                                                            bool no_tighten_factor) {
  // Fast path for when the inputs are not symbolic.
  if (is_const_int(extent) && is_const_int(factor)) {
    // Use conservative division; consts will properly simplify anyway.
    return ConservativeDiv(extent, factor, no_tighten_factor);
  }
  // A special case that can happen in loop splitting.
  if (is_const_int(extent, 1)) {
    // 1 splitted by anything is still (1, 1).
    return {extent, extent};
  }
  extent = Analyzer().canonical_simplify(extent);
  auto [min_factor, divided] = this->SymbolicDiv(extent, factor, no_tighten_factor);
  return {this->AttemptShorten(min_factor), this->AttemptShorten(divided)};
}

Array<SizeVar> VarContext::GetSplitVars(PrimExpr extent, size_t n_splits, bool whole_div) {
  ICHECK(data_ != nullptr);
  auto* data = static_cast<VarContextNode*>(data_.get());
  Array<SizeVar> vars;
  Array<String> var_names;
  for (size_t i = 0; i < n_splits; i++) {
    String name = "sp_" + std::to_string(data->split_counter) + "_" + std::to_string(i);
    vars.push_back(SizeVar(name, DataType::Int(32), Span(), true));
    var_names.push_back(name);
  }
  ++data->split_counter;
  extent = Analyzer().canonical_simplify(extent);
  data->split_groups.push_back(SplitGroup(extent, var_names, whole_div));
  if (!ExprIsConst(extent)) {
    // Don't put non-const extents in the map. They can contain loop variables that are removed in
    // later transformation steps, and we can't keep track of that.
    ICHECK(n_splits == 1 && !whole_div);
    return vars;
  }
  PrimExpr product = 1;
  for (auto& var : vars) {
    product *= var;
  }
  Var quotient = data->MakeAndInsertVar(extent / product, false);
  data->_div_map.emplace(extent, product * quotient);
  return vars;
}

TVM_REGISTER_GLOBAL("arith.VarMapToStr").set_body_typed([](VarContext context) {
  std::ostringstream os;
  os << context->var_to_expr;
  return String(os.str());
});

}  // namespace arith
}  // namespace tvm