/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file auto_scheduler/utils.cc
 * \brief Common utilities.
 */

#include "utils.h"

#include <tvm/arith/egg_simpl.h>
#include <tvm/driver/driver_api.h>
#include <tvm/tir/analysis.h>
#include <tvm/tir/stmt_functor.h>

#include "search_policy/utils.h"

using namespace tvm::transform;
using namespace tvm::tir::transform;

namespace tvm {
namespace auto_scheduler {

NullStream& NullStream::Global() {
  static NullStream stream;
  return stream;
}

Sequential GetGPUCodeGenPasses(const HardwareParams& hw_params, bool verify) {
  auto pass_list = Array<Pass>();
  auto pass_ctx = PassContext::Current();
  bool disable_vectorize = pass_ctx->GetConfig<Bool>("tir.disable_vectorize", Bool(false)).value();
  bool instrument_bound_checkers =
      pass_ctx->GetConfig<Bool>("tir.instrument_bound_checkers", Bool(false)).value();
  Map<String, PrimExpr> gpu_params{
      {"max_shared_memory_per_block", hw_params->max_shared_memory_per_block},
      {"max_local_memory_per_block", hw_params->max_local_memory_per_block},
      {"max_threads_per_block", hw_params->max_threads_per_block},
      {"max_vector_bytes", hw_params->vector_unit_bytes},
      {"max_vthread", hw_params->max_vthread_extent},
  };
  pass_list = Array<Pass>({// Phase 0
                           InjectPrefetch(), StorageFlatten(64, instrument_bound_checkers),
                           // Phase 1
                           NarrowDataType(32), Simplify(), VectorizeLoop(!disable_vectorize),
                           InjectVirtualThread(), StorageRewrite(), Simplify()});
  if (verify) {
    pass_list.push_back(tir::transform::VerifyGPUCode(gpu_params));
  }
  return Sequential(pass_list);
}

Stmt GenerateCodeForState(const SearchTask& task, const State& state, bool is_symbolic,
                          bool print_error, arith::VarContextNode* var_context) {
  te::Schedule sch;
  Array<te::Tensor> tensors;
  std::tie(sch, tensors) = task->compute_dag.ApplySteps(state->transform_steps);
  // When inlining, replace const matrices with const values.
  // Produces wrong IR, but good enough for feature extraction, and
  // can improve the speed of feature extraction/search.  Must be
  // called before ScheduleToModule to have an effect.
  sch = sch.normalize_for_feature_extraction();

  try {
    const std::string& name = "main";
    auto mod = ScheduleToModule(sch, Array<ObjectRef>{tensors.begin(), tensors.end()}, name,
                                std::unordered_map<te::Tensor, te::Buffer>(), var_context);
    if (IsGPUTask(task)) {
      auto optimize = GetGPUCodeGenPasses(task->hardware_params, !is_symbolic);
      optimize(mod);
    }
    const auto& optimize =
        tir::transform::Sequential(Array<tvm::transform::Pass>{tir::transform::Simplify()});
    mod = optimize(std::move(mod));
    PrimFunc prim_func = Downcast<PrimFunc>(mod->Lookup(name));
    return prim_func->body;
  } catch (Error& e) {
    if (print_error) LOG_WARNING << "Failed to generate code: " << e.what() << "\n";
    return Stmt();
  }
}

TVM_REGISTER_GLOBAL("auto_scheduler.GenerateCodeForState")
    .set_body_typed([](const SearchTask& task, const State& state, Bool symbolic) {
      auto state_copy = state;
      bool is_sym = symbolic->value;
      arith::VarContextNode* var_context =
          is_sym ? state_copy.GetVarContext().CopyOnWrite() : nullptr;
      auto code = GenerateCodeForState(
          task, state, is_sym, /* print error for symbolic generation */ is_sym, var_context);
      return Array<ObjectRef>{code, GetRef<arith::VarContext>(var_context)};
    });

TVM_REGISTER_GLOBAL("auto_scheduler.ExtractBackbone").set_body_typed([](const Array<Step>& steps) {
  Array<String> ret;
  for (auto& step : steps) {
    if (step.as<AnnotationStepNode>()) {
      ret.push_back(AnnotationStepNode::record_prefix_str);
    } else if (step.as<FuseStepNode>()) {
      ret.push_back(FuseStepNode::record_prefix_str);
    } else if (step.as<PragmaStepNode>()) {
      ret.push_back(PragmaStepNode::record_prefix_str);
    } else if (step.as<ReorderStepNode>()) {
      ret.push_back(ReorderStepNode::record_prefix_str);
    } else if (step.as<SplitStepNode>()) {
      ret.push_back(SplitStepNode::record_prefix_str);
    } else if (step.as<FollowSplitStepNode>()) {
      ret.push_back(FollowSplitStepNode::record_prefix_str);
    } else if (step.as<FollowFusedSplitStepNode>()) {
      ret.push_back(FollowFusedSplitStepNode::record_prefix_str);
    } else if (step.as<StorageAlignStepNode>()) {
      ret.push_back(StorageAlignStepNode::record_prefix_str);
    } else if (step.as<ComputeAtStepNode>()) {
      ret.push_back(ComputeAtStepNode::record_prefix_str);
    } else if (step.as<ComputeInlineStepNode>()) {
      ret.push_back(ComputeInlineStepNode::record_prefix_str);
    } else if (step.as<ComputeRootStepNode>()) {
      ret.push_back(ComputeRootStepNode::record_prefix_str);
    } else if (step.as<CacheReadStepNode>()) {
      ret.push_back(CacheReadStepNode::record_prefix_str);
    } else if (step.as<CacheWriteStepNode>()) {
      ret.push_back(CacheWriteStepNode::record_prefix_str);
    } else if (step.as<RfactorStepNode>()) {
      ret.push_back(RfactorStepNode::record_prefix_str);
    } else {
      LOG(FATAL) << "Invalid step: " << step;
    }
  }
  return ret;
});

TVM_REGISTER_GLOBAL("auto_scheduler.ExtractConfigDict")
    .set_body_typed([](const Array<Step>& steps) {
      size_t split_count = 0;
      Map<String, Integer> config;
      for (auto& step : steps) {
        if (auto* split = step.as<SplitStepNode>()) {
          for (size_t i = 0; i < split->lengths.size(); ++i) {
            // Name must match the one in sketch_policy_rule.cc
            auto var_name = "sp_" + std::to_string(split_count) + "_" + std::to_string(i);
            auto len = split->lengths[i];
            auto* len_int = len.as<IntImmNode>();
            ICHECK(len_int) << "Split length must be a constant integer";
            config.Set(var_name, Integer(len_int->value));
          }
          split_count += 1;
        } else if (auto* pragma = step.as<PragmaStepNode>()) {
          std::string pstring = pragma->pragma_type, pat = "auto_unroll_max_step";
          if (pstring.substr(0, pat.length()) == pat) {
            auto unroll_size = std::stoi(pstring.substr(pat.length() + 1));  // skip "$"
            auto var_name = "ur_" + std::to_string(pragma->stage_id);
            config.Set(var_name, Integer(unroll_size));
          }
        }
      }
      return config;
    });

TVM_REGISTER_GLOBAL("auto_scheduler.StateFromConfig")
    .set_body_typed([](const SearchTask& con_task, const State& sym_state,
                       const Map<String, Integer>& config) {
      size_t split_count = 0;
      const auto& task_dag = con_task->compute_dag;
      auto state = task_dag->init_state;
      // Create and replay steps, starting from the initial state of concrete task.
      for (auto step : sym_state->transform_steps) {
        if (auto* split = step.as<SplitStepNode>()) {
          auto lengths = split->lengths;
          // The extents are symbolic in the incoming steps, which needs to be replaced with
          // concrete values. It seems that this extent value is not truly used though,
          // so we just set it to 1.
          auto extent = Integer(1);
          for (size_t i = 0; i < lengths.size(); ++i) {
            // Name must match the one in sketch_policy_rule.cc and in ExtractConfigDict()
            auto var_name = "sp_" + std::to_string(split_count) + "_" + std::to_string(i);
            // Occasionally some variable may not appear at all in the features, and they won't
            // be in `config` either. In this case, we just use the default value 1.
            lengths.Set(i, config.Get(var_name).value_or(1));
          }
          step = SplitStep(split->stage_id, split->iter_id, extent, lengths, split->inner_to_outer);
          split_count += 1;
        } else if (auto* pragma = step.as<PragmaStepNode>()) {
          std::string pstring = pragma->pragma_type, pat = "auto_unroll_max_step";
          if (pstring.substr(0, pat.length()) == pat) {
            auto var_name = "ur_" + std::to_string(pragma->stage_id);
            String new_pragma_type =
                pat + "$" + std::to_string(config.Get(var_name).value_or(1)->value);
            step = PragmaStep(pragma->stage_id, pragma->iter_id, new_pragma_type);
          }
        }
        state.CopyOnWrite()->transform_steps.push_back(step);
        StepApplyToState(step, &state, task_dag);
        state = task_dag.InferBound(state);
      }
      return state;
    });

TVM_REGISTER_GLOBAL("auto_scheduler.MeasurePerformance")
    .set_body_typed([](const SearchPolicy& policy, const ProgramMeasurer& measurer,
                       const Array<State>& states) {
      auto search_task = policy->search_task;
      Array<MeasureInput> m_inputs;
      for (auto& st : states) {
        m_inputs.push_back(MeasureInput(search_task, st));
      }
      auto results = measurer->Measure(search_task, policy, m_inputs);
      ICHECK(results.size() == m_inputs.size());
      return results;
    });

}  // namespace auto_scheduler
}  // namespace tvm
