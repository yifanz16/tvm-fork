#ifndef TVM_AUTO_SCHEDULER_SEARCH_POLICY_INIT_RULES_SYM_H_
#define TVM_AUTO_SCHEDULER_SEARCH_POLICY_INIT_RULES_SYM_H_

#include <tvm/auto_scheduler/loop_state.h>
#include <tvm/auto_scheduler/search_task.h>

#include <string>
#include <utility>
#include <vector>

#include "utils.h"

namespace tvm {
namespace auto_scheduler {

class SketchPolicyNode;

/********** Init Population **********/

class SymAnnotRule {
 public:
  /*!
   * \brief Apply function of this rule.
   * \param policy The SketchPolicyNode of this rule, some member may get changed during the
   * rule applying. (e.g. random number generator)
   * \param state The state to apply this rule, update inplace.
   * \return The result of this rule, indicate if there's any valid state generated.
   */
  virtual std::vector<State> Apply(const SketchPolicyNode* policy, State state) const = 0;

  virtual ~SymAnnotRule() = default;
};

// A helper to define population initialization rules
#define DEFINE_SYM_ANNOTATION_RULE(rule_name)                                      \
  class rule_name : public SymAnnotRule {                                          \
   public:                                                                         \
    std::vector<State> Apply(const SketchPolicyNode* policy, State state) const final; \
  };

/*! \brief The rule that fills the incomplete SplitSteps. */
DEFINE_SYM_ANNOTATION_RULE(SymInitFillTileSize);

/*! \brief The rule that randomly changes the computation location for some stages that do not
 * need tiling and are not strictly inlineable(e.g. data padding). */
DEFINE_SYM_ANNOTATION_RULE(SymInitChangeComputeLocation);

/*! \brief The rule that annotates parallel for CPU. */
DEFINE_SYM_ANNOTATION_RULE(SymInitParallel);

/*! \brief The rule that annotates unroll. */
DEFINE_SYM_ANNOTATION_RULE(SymInitUnroll);

/*! \brief The rule that annotates vectorization. */
DEFINE_SYM_ANNOTATION_RULE(SymInitVectorization);

/*! \brief The rule that annotates thread binding for GPU. */
DEFINE_SYM_ANNOTATION_RULE(SymInitThreadBind);

}  // namespace auto_scheduler
}  // namespace tvm

#endif  // TVM_AUTO_SCHEDULER_SEARCH_POLICY_INIT_RULES_SYM_H_
