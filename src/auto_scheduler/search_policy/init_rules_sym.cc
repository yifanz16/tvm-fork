/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/*!
 * \file auto_scheduler/search_policy/sketch_policy_rules.cc
 * \brief Rules for generating the sketches, sampling the initial population, and mutating the
 * population in SketchPolicy.
 */

#include "init_rules_sym.h"

#include <tvm/arith/var_context.h>

#include <set>
#include <string>
#include <utility>
#include <vector>

#include "sketch_policy.h"

namespace tvm {
namespace auto_scheduler {

static SymInitFillTileSize init_fill_tile_size;
static SymInitChangeComputeLocation init_change_compute_location;
static SymInitParallel init_parallel;
static SymInitUnroll init_unroll;
static SymInitVectorization init_vectorization;
static SymInitThreadBind init_thread_bind;

/********** Init Population **********/

std::vector<State> SymInitFillTileSize::Apply(const SketchPolicyNode* policy, State state) const {
  // TODO: remember variable range constraints
  // int max_innermost_split_factor =
  //     GetIntParam(policy->params, SketchParamKey::max_innermost_split_factor);
  // Scan the transformation history and randomly fill tiles size for all SplitStep
  Array<Step> new_steps;
  for (auto& step : state->transform_steps) {
    if (auto ps = step.as<SplitStepNode>()) {
      ICHECK(ps->extent);
      bool any_defined = std::any_of(ps->lengths.begin(), ps->lengths.end(),
                                     [](const ObjectRef& item) { return item.defined(); });
      auto vars = state.GetVarContext().GetSplitVars(ps->extent.value(), ps->lengths.size(), true);
      Array<PrimExpr> new_split_lengths(vars.begin(), vars.end());
      if (any_defined) {
        LOG_WARNING << "Replacing already defined split sizes " << ps->lengths << " with "
                    << new_split_lengths;
      }
      new_steps.push_back(
          SplitStep(ps->stage_id, ps->iter_id, ps->extent, new_split_lengths, ps->inner_to_outer));
    } else {
      new_steps.push_back(step);
    }
  }
  StateNode* ret_node = state.CopyOnWrite();
  ret_node->concrete = true;
  ret_node->transform_steps = new_steps;
  return {GetRef<State>(ret_node)};
}

// CCL -> Change Compute Location
void CCLGetNextStates(const SearchTask& task, const State& state, int stage_id,
                      std::vector<State>& ret) {
  const Stage& stage = state->stages[stage_id];
  // Skip the inlined stages and placeholders
  if (stage->op_type == StageKind::kPlaceholder || stage->compute_at == ComputeAtKind::kInlined) {
    ret.push_back(state);
    return;
  }
  // Skip the tiled stages
  if (IsTiled(stage) || NeedsMultilevelTiling(task, state, stage_id)) {
    ret.push_back(state);
    return;
  }

  std::vector<std::pair<int, int>> candidates = GetComputeLocationCandidates(task, state, stage_id);
  if (!HasReduceIter(stage)) {
    State inline_ = state;
    const auto& map = inline_->attach_map->stage_to_attach_iter;
    if (map.find(stage_id) != map.end()) {
      inline_.compute_inline(stage_id);
      ret.push_back(inline_);
    }
  }
  State root = state;
  root.compute_root(stage_id);
  ret.push_back(root);
  for (auto [target_stage_id, iter_id] : candidates) {
    State compute_at = state;
    auto& stage = compute_at->stages[target_stage_id];
    compute_at.compute_at(stage_id, target_stage_id, stage->iters[iter_id]);
    ret.push_back(compute_at);
  }
}

std::vector<State> SymInitChangeComputeLocation::Apply(const SketchPolicyNode* policy,
                                                       State state) const {
  if (GetIntParam(policy->params, SketchParamKey::disable_change_compute_location)) {
    return {state};
  }
  // ChangeComputeLocation does not change the number of stages.
  // We are essentially doing a BFS on a tree with depth n_stages,
  // using a for-loop that ticks down: `stage_id in range(n_stages - 1, -1, -1)`.
  int n_stages = static_cast<int>(state->stages.size());
  auto& task = policy->search_task;
  std::vector<State> cur_states{state}, next_states{};  // All states at current stage_id
  for (int stage_id = n_stages - 1; stage_id >= 0; --stage_id) {
    for (auto& state : cur_states) {
      auto& stages = state->stages;
      ICHECK(static_cast<int>(stages.size()) == n_stages);
      const Stage& stage = stages[stage_id];
      CCLGetNextStates(task, state, stage_id, next_states);
    }
    cur_states.swap(next_states);
    next_states.clear();
  }

  std::vector<State> ret;
  // InferBound has a vector<State> -> vector<State> variant
  // but it is parallelized and our VarContext isn't thread-safe.
  for (auto& state : cur_states) {
    try {
      ret.push_back(task->compute_dag.InferBound(state));
    } catch (Error& e) {
      LOG_WARNING << "InferBound fails on the state:\n"
                  << state << "\n"
                  << "with: " << e.what() << std::endl;
    }
  }
  return ret;
}

void AnnotateParallel(const SearchTask& task, State& state, int stage_id, int iter_offset) {
  const Stage& stage = state->stages[stage_id];
  auto& attached_stages = state->attach_map->iter_to_attached_stages;
  Array<Iterator> to_fuse;
  PrimExpr parallel_degree = Integer(1);
  bool any_parallel = false;
  // Try to fuse and parallel the outermost n iterators
  // Stop if we meet reduce iterator or we have enough parallel degree
  size_t iter_id = iter_offset;
  for (; iter_id < stage->iters.size(); ++iter_id) {
    const Iterator& it = stage->iters[iter_id];
    if (it->iter_kind == IteratorKind::kReduction || it->annotation != IteratorAnnotation::kNone) {
      break;
    }
    to_fuse.push_back(it);
    parallel_degree *= it->range->extent;
    any_parallel = true;
    // TODO: try to generate multiple sketches from this condition?
    // if (parallel_degree > policy.search_task->hardware_params->num_cores * 16) {
    //   break;
    // }
    if (attached_stages.count({stage_id, iter_id})) {
      break;
    }
  }

  if (!any_parallel) {
    auto res = attached_stages.find({stage_id, iter_id});
    if (res != attached_stages.end()) {
      for (int attached_stage_id : res->second) {
        AnnotateParallel(task, state, attached_stage_id, 0);
      }
      AnnotateParallel(task, state, stage_id, iter_id + 1);
    }
  }

  if (!to_fuse.empty()) {
    State ret = state;
    if (to_fuse.size() == 1) {
      ret.parallel(stage_id, to_fuse[0]);
    } else {
      Iterator fused_iter = ret.fuse(stage_id, to_fuse);
      ret.parallel(stage_id, fused_iter);
    }
  }
}

std::vector<State> SymInitParallel::Apply(const SketchPolicyNode* policy, State state) const {
  for (size_t stage_id = 0; stage_id < state->stages.size(); ++stage_id) {
    const Stage& stage = state->stages[stage_id];
    if (stage->compute_at != ComputeAtKind::kRoot || stage->op_type == StageKind::kPlaceholder) {
      continue;
    }
    AnnotateParallel(policy->search_task, state, stage_id, 0);
  }
  return {state};
}

std::vector<State> SymInitUnroll::Apply(const SketchPolicyNode* policy, State state) const {
  for (size_t stage_id = 0; stage_id < state->stages.size(); ++stage_id) {
    const Stage& stage = state->stages[stage_id];
    // Skip the inlined stage and placeholder stage
    if (stage->compute_at == ComputeAtKind::kInlined || stage->op_type == StageKind::kPlaceholder) {
      continue;
    }

    // Handle always_unroll_inner attr
    if (stage->op->attrs.count(SearchPolicyKey::always_unroll_inner)) {
      const auto& to_unroll_name_set =
          GetIterNameSetParam(stage->op->attrs, SearchPolicyKey::always_unroll_inner);

      // Unroll the space iterators and reduce iterators listed in the attrs in the innermost
      // tile
      std::set<std::string> visited_names;
      for (int n = static_cast<int>(stage->iters.size()) - 1; n >= 0; n--) {
        const Iterator& it = stage->iters[n];

        // If we meet two iterators that come from a same original iterator,
        // then we are out of the innermost tile
        size_t size_before = visited_names.size();
        ExtractOriginalIterators(it->name, &visited_names);
        if (size_before == visited_names.size()) {
          break;
        }

        std::set<std::string> name;
        ExtractOriginalIterators(it->name, &name);
        if (name.size() == 1 && to_unroll_name_set.count(*name.begin())) {
          if (it->annotation == IteratorAnnotation::kNone) {
            state.unroll(stage_id, it);
          }
        }
      }
    }

    if (HasReduceIter(stage)) {
      // Use auto unroll for multi level tiled stage
      auto var_name = "ur_" + std::to_string(stage_id);
      state.pragma(stage_id, state->stages[stage_id]->iters[0],
                   std::string("auto_unroll_max_step") + "$" + var_name);
    }
  }

  return {state};
}

void VecGetNextStates(const SketchPolicyNode* policy, const State& state, int stage_id,
                      std::vector<State>& out) {
  const Stage& stage = state->stages[stage_id];
  // Skip the inlined stage and placeholder stage
  if (stage->compute_at == ComputeAtKind::kInlined || stage->op_type == StageKind::kPlaceholder) {
    out.push_back(state);
    return;
  }
  // Try to fuse and vectorize the space iterators in the inner most tile
  PrimExpr cum_length_prod = Integer(1);
  int num_fusible = 0;
  int n_iters = static_cast<int>(stage->iters.size());
  while (num_fusible < n_iters) {
    int iter_id = n_iters - 1 - num_fusible;
    // Stop if this iterator has been a compute at attach point
    if (state->attach_map->iter_to_attached_stages.count(std::make_pair(stage_id, iter_id))) {
      break;
    }
    const Iterator& it = stage->iters[iter_id];
    // Stop if we meet a reduce iterator or annotated iterator
    if (it->iter_kind == IteratorKind::kReduction || it->annotation != IteratorAnnotation::kNone) {
      break;
    }
    // Stop if the memory access is not continuous (vectorizable)
    // Note: The check is too hard, so we use heuristic here
    if (IsTiled(stage) && num_fusible != 0) {
      // If the stage is tiled, then the memory access must not be continuous
      // for the innermost two iterators
      break;
    }
    cum_length_prod *= it->range->extent;
    // if (cum_length_prod > GetIntParam(policy->params, SketchParamKey::max_vectorize_size)) {
    //   break;
    // }
    num_fusible++;
  }

  if (num_fusible == 0) {
    out.push_back(state);
    return;
  }
  State fuse1 = state;
  fuse1.vectorize(stage_id, stage->iters.back());
  out.push_back(fuse1);
  for (int fuse_i = 2; fuse_i <= num_fusible; ++fuse_i) {
    State fuse_n = state;
    Array<Iterator> to_fuse(stage->iters.end() + (-fuse_i), stage->iters.end());
    fuse_n.vectorize(stage_id, fuse_n.fuse(stage_id, to_fuse));
    out.push_back(fuse_n);
  }
}

std::vector<State> SymInitVectorization::Apply(const SketchPolicyNode* policy, State state) const {
  std::vector<State> cur_states{state}, next_states{};  // All states at current 0
  size_t n_stages = state->stages.size();
  for (size_t stage_id = 0; stage_id < n_stages; ++stage_id) {
    for (auto& state : cur_states) {
      auto& stages = state->stages;
      ICHECK(stages.size() == n_stages);
      const Stage& stage = stages[stage_id];
      VecGetNextStates(policy, state, stage_id, next_states);
    }
    cur_states.swap(next_states);
    next_states.clear();
  }
  return cur_states;
}

std::vector<State> SymInitThreadBind::Apply(const SketchPolicyNode* policy, State state) const {
  // Collect all stages that are roots of stages that perform multi-level tiling.
  auto& task = policy->search_task;
  std::set<int> multi_level_tiling_root_set;
  for (size_t stage_id = 0; stage_id < state->stages.size(); ++stage_id) {
    if (NeedsMultilevelTiling(task, state, stage_id)) {
      const Stage& stage = state->stages[stage_id];
      if (stage->compute_at == ComputeAtKind::kInlined) {
        continue;
      } else if (stage->compute_at != ComputeAtKind::kIter) {
        // This stage is not multi-level tiled,
        // so it must be produced by RuleCrossThreadReduction.
        ICHECK(HasCrossThreadReduction(state, stage_id));
      } else {
        const auto res = state->attach_map->stage_to_attach_iter.find(stage_id);
        ICHECK(res != state->attach_map->stage_to_attach_iter.end());
        multi_level_tiling_root_set.insert(res->second.first);
      }
    }
  }

  state = task->compute_dag.InferBound(state);
  std::vector<std::pair<size_t, String>> thread_bound_iters, vectorized_iters;
  for (int stage_id = state->stages.size() - 1; stage_id >= 0; --stage_id) {
    const Stage& stage = state->stages[stage_id];

    if (stage->compute_at == ComputeAtKind::kInlined || stage->op_type == StageKind::kPlaceholder) {
      continue;
    }

    // Deal with the cross-thread reduction generated by RuleCrossThreadReduction
    if (HasCrossThreadReduction(state, stage_id)) {
      if (stage->compute_at != ComputeAtKind::kRoot) {
        continue;
      }

      Iterator fused_it;
      state = std::move(FuseAllOuterSpaceIterators(state, stage_id, &fused_it));
      state.bind(stage_id, fused_it, IteratorAnnotation::kBlockX);
      continue;
    }

    // Skip if this stage has already been annotaed with threadIdx.x
    if (HasAnnotatedIter(stage, IteratorAnnotation::kThreadX)) {
      continue;
    }

    if (stage->compute_at == ComputeAtKind::kRoot) {
      // This stage has not been tiled, but in GPU schedule, we must tile the root stage
      // to do thread binding
      if (!multi_level_tiling_root_set.count(stage_id)) {
        Iterator fused_it;
        state = FuseAllOuterSpaceIterators(state, stage_id, &fused_it);
        auto size_var = state.GetVarContext().GetSplitVars(fused_it->range->extent, 1, false)[0];
        const auto& split_its = state.split(stage_id, fused_it, {size_var});
        state.bind(stage_id, split_its[0], IteratorAnnotation::kBlockX);
        state.bind(stage_id, split_its[1], IteratorAnnotation::kThreadX);
        thread_bound_iters.emplace_back(stage_id, split_its[1]->name);
        continue;
      }

      // Otherwise, this is a tiled root stage, we assume it should be tiled with 3 space level
      // in the outer iterators.
      // The remaining part deals with the thread binding for multi-level tiled stages
      auto pop = stage->op.as<te::ComputeOpNode>();
      std::vector<Iterator> to_fuse;

      // Fuse the outermost space tile as blockIdx
      for (size_t i = 0; i < pop->axis.size(); i++) {
        const auto& it = state->stages[stage_id]->iters[i];
        // There may be some iterators that are marked with no split, stop if reaches next
        // tiling level
        if (!StrEndsWith(it->name, ".0")) {
          break;
        }
        to_fuse.push_back(it);
      }
      const auto& blockidx_it = state.fuse(stage_id, to_fuse);
      state.bind(stage_id, blockidx_it, IteratorAnnotation::kBlockX);

      // Fuse the second outermost space tile as vthread
      to_fuse.clear();
      for (size_t i = 1; i < pop->axis.size() + 1; i++) {
        const auto& it = state->stages[stage_id]->iters[i];
        // There may be some iterators that are marked with no split, stop if reaches next
        // tiling level
        if (!StrEndsWith(it->name, ".1")) {
          break;
        }
        to_fuse.push_back(state->stages[stage_id]->iters[i]);
      }
      const auto& vthread_it = state.fuse(stage_id, to_fuse);
      state.bind(stage_id, vthread_it, IteratorAnnotation::kVThread);

      // Fuse the third outermost space tile as threadIdx
      to_fuse.clear();
      for (size_t i = 2; i < pop->axis.size() + 2; i++) {
        const auto& it = state->stages[stage_id]->iters[i];
        // There may be some iterators that are marked with no split, stop if reaches next
        // tiling level
        if (!StrEndsWith(it->name, ".2")) {
          break;
        }
        to_fuse.push_back(state->stages[stage_id]->iters[i]);
      }
      const auto& threadidx_it = state.fuse(stage_id, to_fuse);
      state.bind(stage_id, threadidx_it, IteratorAnnotation::kThreadX);
      thread_bound_iters.emplace_back(stage_id, threadidx_it->name);
    } else if (stage->compute_at == ComputeAtKind::kIter &&
               StrEndsWith(stage->op->name, ".shared")) {
      // Do cooperative fetching for the cache read stage.
      // Get spatial_split_step_ids from the root stage
      const auto& it = state->attach_map->stage_to_attach_iter.find(stage_id);
      ICHECK(it != state->attach_map->stage_to_attach_iter.end());
      Array<Integer> spatial_split_step_ids = GetSpatialSplitStepIds(state, it->second.first);

      // Fuse all iterators to do cooperative fetching
      Iterator fused = state.fuse(stage_id, state->stages[stage_id]->iters);
      // Split out an extra iterator for vectorization
      auto size_var = state.GetVarContext().GetSplitVars(fused->range->extent, 1, false)[0];
      const auto& iters0 = state.split(stage_id, fused, {size_var});
      state.vectorize(stage_id, iters0[1]);
      vectorized_iters.emplace_back(stage_id, iters0[1]->name);
      // Follow split to keep a same thread extent with the root stage
      const auto& iters1 =
          state.follow_fused_split(stage_id, iters0[0], spatial_split_step_ids, 1, true);
      state.bind(stage_id, iters1[1], IteratorAnnotation::kThreadX);
      // This is followsplit, which has the same extent as the root stage.
      // No need to put this in thread_bound_iters.
    }
  }
  return {state};
}

void PrintSketch(const State& sketch_) {
  auto& info = LOG_INFO;
  info << "Initial sketch " << sketch_ << "\n\n";
  info << "Transform steps: \n";
  for (auto& step : sketch_->transform_steps) {
    info << "  " << PrintTrStep(step) << "\n";
  }
  info << "\n";
}

Array<State> GetExtendedSketches(const SketchPolicy& sketch_policy, State sketch) {
  std::vector<SymAnnotRule*> sym_annot_rules;
  auto& task = sketch_policy->search_task;
  if (IsCPUTask(task)) {
    sym_annot_rules.push_back(&init_fill_tile_size);
    sym_annot_rules.push_back(&init_change_compute_location);
    sym_annot_rules.push_back(&init_parallel);
    sym_annot_rules.push_back(&init_unroll);
    // sym_annot_rules.push_back(&init_vectorization);
  } else if (IsGPUTask(task)) {
    sym_annot_rules.push_back(&init_fill_tile_size);
    sym_annot_rules.push_back(&init_thread_bind);
    sym_annot_rules.push_back(&init_unroll);
  }

  // Every sketch must try each rule once.
  std::vector<State> sketches{sketch}, next_sketches{};
  auto* sk_policy_node = sketch_policy.operator->();
  // std::cout << "!/ Symbolic config generation from sketch: \n";
  // int i = 0;
  // PrintSketch(sketch);
  for (const auto* rule : sym_annot_rules) {
    // std::cout << "Applying rule " << i << "\n";
    for (const auto& sketch : sketches) {
      auto ret = rule->Apply(sk_policy_node, sketch);
      next_sketches.insert(next_sketches.end(), ret.begin(), ret.end());
    }
    sketches = std::move(next_sketches);
    next_sketches = {};
    // std::cout << "Now has " << sketches.size() << " sketches\n";
    // ++i;
    // std::cout << "\n\n";
  }

  return Array<State>(sketches.begin(), sketches.end());
}

TVM_REGISTER_GLOBAL("auto_scheduler.GetExtendedSketches")
    .set_body_typed([](SketchPolicy policy, Array<State> sketches) {
      Array<Array<State>> ret;
      for (auto& sketch : sketches) {
        auto states = GetExtendedSketches(policy, sketch);
        ret.push_back(states);
      }
      return ret;
    });

}  // namespace auto_scheduler
}  // namespace tvm
