#include <tvm/auto_scheduler/search_task.h>

#include "utils.h"

namespace tvm {
namespace auto_scheduler {

String PrintTrStep(const Step& step) {
  std::ostringstream os;
  if (auto ps = step.as<AnnotationStepNode>()) {
    os << "Annotation(stage_id=" << ps->stage_id << ", loop=" << ps->iter_id << ", annotation=\""
       << IteratorAnnotationString[static_cast<int>(ps->annotation)] << "\")";
  } else if (auto ps = step.as<FuseStepNode>()) {
    os << "Fuse(stage_id=" << ps->stage_id << ", fused_ids=" << ps->fused_ids << ")";
  } else if (auto ps = step.as<PragmaStepNode>()) {
    os << "Pragma(stage_id=" << ps->stage_id << ", loop=" << ps->iter_id
       << ", pragma=" << ps->pragma_type << ")";
  } else if (auto ps = step.as<ReorderStepNode>()) {
    os << "Reorder(stage_id=" << ps->stage_id << ", order_after=" << ps->after_ids << ")";
  } else if (auto ps = step.as<SplitStepNode>()) {
    os << "Split(stage_id=" << ps->stage_id << ", loop=" << ps->iter_id << ", extent=" << ps->extent
       << ", " << ps->lengths << ", inner_to_outer=" << ps->inner_to_outer << ")";
  } else if (auto ps = step.as<FollowSplitStepNode>()) {
    os << "FollowSplit(stage_id=" << ps->stage_id << ", loop=" << ps->iter_id
       << ", src_step_id=" << ps->src_step_id << ", n_split=" << ps->n_split << ")";
  } else if (auto ps = step.as<FollowFusedSplitStepNode>()) {
    os << "FollowFusedSplit(stage_id=" << ps->stage_id << ", loop=" << ps->iter_id
       << ", src_step_ids=" << ps->src_step_ids << ", level=" << ps->level
       << ", factor_or_nparts=" << ps->factor_or_nparts << ")";
  } else if (auto ps = step.as<StorageAlignStepNode>()) {
    os << "StorageAlign(stage_id=" << ps->stage_id << ", loop=" << ps->iter_id
       << ", factor=" << ps->factor << ", offset=" << ps->offset << ")";
  } else if (auto ps = step.as<ComputeAtStepNode>()) {
    os << "ComputeAt(stage_id=" << ps->stage_id << ", target_stage_id=" << ps->target_stage_id
       << ", loop=" << ps->target_iter_id << ")";
  } else if (auto ps = step.as<ComputeInlineStepNode>()) {
    os << "ComputeInline(stage_id=" << ps->stage_id << ")";
  } else if (auto ps = step.as<ComputeRootStepNode>()) {
    os << "ComputeRoot(stage_id=" << ps->stage_id << ")";
  } else if (auto ps = step.as<CacheReadStepNode>()) {
    os << "CacheRead(stage_id=" << ps->stage_id << ", scope_name=" << ps->scope_name
       << ", reader_stage_ids=" << ps->reader_stage_ids << ")";
  } else if (auto ps = step.as<CacheWriteStepNode>()) {
    os << "CacheWrite(stage_id=" << ps->stage_id << ", scope_name=" << ps->scope_name << ")";
  } else if (auto ps = step.as<RfactorStepNode>()) {
    os << "RFactor(stage_id=" << ps->stage_id << ", from_loop=" << ps->iter_id
       << ", to_loop=" << ps->factor_iter_id << ")";
  } else {
    LOG(FATAL) << "Invalid step: " << step;
  }
  return os.str();
}

std::vector<std::pair<State, Stmt>> ReplayStepsAndGenCode(const SearchTask& task,
                                                          const Array<Step>& steps) {
  const auto& task_dag = task->compute_dag;
  auto state = task_dag->init_state;
  // Returns N + 1 pieces of code: the first one is the code before the first step.
  std::vector<std::pair<State, Stmt>> ret;
  auto GenerateAndEmplace = [&task, &ret](const State& state) {
    auto node_ptr = make_object<StateNode>(*state.get());  // copy
    auto code = GenerateCodeForState(task, state, false);
    ret.emplace_back(State(node_ptr), code);
  };
  GenerateAndEmplace(state);
  for (auto& step : steps) {
    state.CopyOnWrite()->transform_steps.push_back(step);
    StepApplyToState(step, &state, task_dag);
    state = task_dag.InferBound(state);
    GenerateAndEmplace(state);
  }
  return ret;
}

TVM_REGISTER_GLOBAL("auto_scheduler.PrintTrStep").set_body_typed(PrintTrStep);

TVM_REGISTER_GLOBAL("auto_scheduler.ReplayStepsAndGenCode")
    .set_body_typed([](SearchTask task, Array<Step> steps) {
      Array<Array<ObjectRef>> ret;
      for (const auto& p : ReplayStepsAndGenCode(task, steps)) {
        ret.push_back(Array<ObjectRef>{p.first, p.second});
      }
      return ret;
    });

TVM_REGISTER_GLOBAL("auto_scheduler.EncodeTrSteps").set_body_typed([](const Array<Step>& steps) {
  std::ostringstream os;
  dmlc::JSONWriter writer(&os);
  writer.Write(steps);
  return os.str();
});

}  // namespace auto_scheduler
}  // namespace tvm

namespace dmlc {
namespace json {
template <>
struct Handler<::tvm::Array<::tvm::auto_scheduler::Step>> {
  inline static void Write(dmlc::JSONWriter* writer,
                           const ::tvm::Array<::tvm::auto_scheduler::Step>& data) {
    writer->BeginArray(false);
    for (const auto& step : data) {
      writer->WriteArraySeperator();
      writer->BeginArray(false);
      step->WriteToRecord(writer);
      writer->EndArray();
    }
    writer->EndArray();
  }

  inline static void Read(dmlc::JSONReader* reader,
                          ::tvm::Array<::tvm::auto_scheduler::Step>* data) {
    bool s;
    reader->BeginArray();
    data->clear();
    while (reader->NextArrayItem()) {
      reader->BeginArray();
      data->push_back(::tvm::auto_scheduler::StepReadFromRecord(reader));
      s = reader->NextArrayItem();
      ICHECK(!s);
    }
  }
};
}  // namespace json
}  // namespace dmlc
