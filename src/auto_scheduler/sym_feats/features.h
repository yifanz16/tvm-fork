#ifndef TVM_AUTO_SCHEDULER_FEATURES_H_
#define TVM_AUTO_SCHEDULER_FEATURES_H_

#include <tvm/auto_scheduler/search_task.h>
#include <tvm/tir/stmt.h>

#include <unordered_map>

namespace tvm {
namespace auto_scheduler {

template <class T>
using BufferMap = std::unordered_map<tir::Buffer, T, ObjectHash, ObjectEqual>;
template <class T>
using ExprMap = std::unordered_map<PrimExpr, T, StructuralHash, StructuralEqual>;

std::vector<PrimExpr> GetPerStoreFeatureExpr(Stmt stmt, arith::VarContextNode& context,
                                             int cache_line_size, int max_n_bufs);

std::vector<PrimExpr> GetConstraints(const Stmt& code, const HardwareParams& hw_params);

}  // namespace auto_scheduler
}  // namespace tvm

#endif  // TVM_AUTO_SCHEDULER_FEATURE_H_
