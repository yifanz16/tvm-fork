#include <tvm/arith/egg_simpl.h>
#include <tvm/arith/var_context.h>
#include <tvm/auto_scheduler/feature.h>
#include <tvm/support/parallel_for.h>

#include <optional>

#include "features.h"
#include "rangeinfer.h"

namespace tvm {
namespace auto_scheduler {

using namespace tvm::arith;

std::unordered_map<int, double> Factorize(int n) {
  std::unordered_map<int, double> factors;
  for (int i = 2; i <= n; ++i) {
    while (n % i == 0) {
      factors[i] += 1;
      n /= i;
    }
  }
  return factors;
}

using DecompT = std::unordered_map<std::string, std::unordered_map<size_t, SizeVar>>;

class LinearExprNode : public Object {
 public:
  Map<Var, FloatImm> lin_terms;
  FloatImm constant;

  void VisitAttrs(AttrVisitor* v) {
    v->Visit("lin_terms", &lin_terms);
    v->Visit("constant", &constant);
  }

  static constexpr const char* _type_key = "ansor.LinearExpr";
  TVM_DECLARE_FINAL_OBJECT_INFO(LinearExprNode, Object);
};

TVM_REGISTER_NODE_TYPE(LinearExprNode);

class LinearExpr : public ObjectRef {
 public:
  LinearExpr(double constant) {
    auto node = make_object<LinearExprNode>();
    node->constant = ToFloatImm(constant);
    this->data_ = std::move(node);
  }

  LinearExpr(Var var) {
    auto node = make_object<LinearExprNode>();
    node->constant = ToFloatImm(0.0f);
    node->lin_terms.Set(var, ToFloatImm(1.0f));
    this->data_ = std::move(node);
  }

  LinearExpr(double constant, const std::unordered_map<std::string, double>& name2coef) {
    auto node = make_object<LinearExprNode>();
    node->constant = ToFloatImm(0.0f);
    for (auto& [name, coef] : name2coef) {
      node->lin_terms.Set(tvm::tir::Var(name), ToFloatImm(coef));
    }
    this->data_ = std::move(node);
  }

  PrimExpr ToPrimExpr() const {
    auto node = this->operator->();
    PrimExpr ret = node->constant;
    for (auto& [var, coef] : node->lin_terms) {
      ret = ret + var * coef;
    }
    return ret;
  }

  LinearExpr operator+(const LinearExpr& other) const {
    if (!this->defined() || !other.defined()) {
      return LinearExpr();
    }
    auto ret = make_object<LinearExprNode>();
    auto this_ = this->operator->();
    ret->constant = Downcast<FloatImm>(this_->constant + other->constant);
    ret->lin_terms = this_->lin_terms;
    for (auto& [var, coef1] : other->lin_terms) {
      auto coef2 = ret->lin_terms.Get(var).value_or(ToFloatImm(0.0f));
      ret->lin_terms.Set(var, Downcast<FloatImm>(coef1 + coef2));
    }
    return LinearExpr(ret);
  }

  LinearExpr operator*(FloatImm other) {
    if (!this->defined() || !other.defined()) {
      return LinearExpr();
    }
    auto ret = make_object<LinearExprNode>();
    auto this_ = this->operator->();
    ret->constant = Downcast<FloatImm>(this_->constant * other);
    for (auto& [var, coef] : this_->lin_terms) {
      ret->lin_terms.Set(var, Downcast<FloatImm>(coef * other));
    }
    return LinearExpr(ret);
  }
  LinearExpr operator*(LinearExpr other) {
    if (!this->defined() || !other.defined()) {
      return LinearExpr();
    } else if ((*this)->lin_terms.empty()) {
      return other * (*this)->constant;
    } else if (other->lin_terms.empty()) {
      return (*this) * other->constant;
    } else {
      return LinearExpr();
    }
  }

  TVM_DEFINE_OBJECT_REF_METHODS(LinearExpr, ObjectRef, LinearExprNode);
};

TVM_STATIC_IR_FUNCTOR(ReprPrinter, vtable)
    .set_dispatch<LinearExprNode>([](const ObjectRef& node, ReprPrinter* p) {
      auto* expr = static_cast<const LinearExprNode*>(node.get());
      auto& os = p->stream;
      os << "(" << expr->constant << "; ";
      bool first = true;
      for (auto& [var, coef] : expr->lin_terms) {
        if (!first) {
          os << ", ";
        }
        os << "(" << coef << " * " << var << ")";
        first = false;
      }
      os << ")";
    });

class LinExprExtractor : public ExprFunctor<LinearExpr(const PrimExpr&)> {
  LinearExpr VisitExpr_(const VarNode* e) override { return LinearExpr(GetRef<Var>(e)); }
  LinearExpr VisitExpr_(const IntImmNode* e) override { return LinearExpr((double)e->value); }
  LinearExpr VisitExpr_(const FloatImmNode* e) override { return LinearExpr(e->value); }

  LinearExpr VisitExpr_(const MulNode* e) override { return VisitExpr(e->a) * VisitExpr(e->b); }
  LinearExpr VisitExpr_(const DivNode* e) override {
    auto lhs = VisitExpr(e->a), rhs = VisitExpr(e->b);
    if (!lhs.defined() || !rhs.defined()) {
      return LinearExpr();
    }
    if (rhs->lin_terms.empty()) {
      return lhs * Downcast<FloatImm>(div(ToFloatImm(1.0f), rhs->constant));
    } else {
      return LinearExpr();
    }
  }
  LinearExpr VisitExpr_(const AddNode* e) override { return VisitExpr(e->a) + VisitExpr(e->b); }
  LinearExpr VisitExpr_(const SubNode* e) override {
    return VisitExpr(e->a) + VisitExpr(e->b) * ToFloatImm(-1.0f);
  }
  LinearExpr VisitExpr_(const CastNode* e) override { return VisitExpr(e->value); }

  LinearExpr VisitExprDefault_(const Object* e) override { return LinearExpr(); }
};

class ExpTransform {
  static const int DEFAULT_UBOUND = 10;

 public:
  ExpTransform(VarMapT sizes_, const std::vector<SplitGroup>& split_groups)
      : sizes(std::move(sizes_)) {
    // For variables already in `split_groups`, compute their decomposition now.
    // For others, decompose them later on the fly.
    for (const auto& group : split_groups) {
      this->DecomposeAndInsert(group);
    }
  }

  VarMapT GetVarMap() const {
    VarMapT ret;
    for (const auto& [v1, decomp] : this->var_decomp) {
      PrimExpr decomp_expr = Integer(1);
      for (const auto& [prime, v2] : decomp) {
        decomp_expr *= generic_pow(Integer(prime), v2);
      }
      ret[v1] = decomp_expr;
    }
    return ret;
  }

  std::pair<std::vector<std::string>, std::vector<PrimExpr>> GetVarsAndConstraints() const {
    std::vector<std::string> vars;
    std::vector<PrimExpr> cons = this->factor_cons;
    for (const auto& [v1, decomp] : this->var_decomp) {
      for (const auto& [prime, v2] : decomp) {
        vars.push_back(v2->name_hint);
        cons.push_back(greater_equal(v2, 0));
      }
    }
    return {vars, cons};
  }

  void RunOnExpr(const PrimExpr& e, const OrderedVarMap& vmap) {
    auto CollectVars = [this, &vmap](const ObjectRef& e) {
      if (auto var = e.as<VarNode>()) {
        auto name = var->name_hint;
        if (!vmap.contains(name) && this->sizes.count(name) == 0) {
          this->DecomposeVar(name);
        }
      }
    };
    PostOrderVisit(e, CollectVars);
  }

 private:
  void DecomposeVar(const String& vname) {
    auto it = this->var_decomp.find(vname);
    if (it == this->var_decomp.end()) {
      auto new_name = vname + "_2";
      SizeVar var(new_name, DataType::Int(32), Span(), true);
      this->var_decomp[vname][2] = var;
    }
  }

  void DecomposeAndInsert(const SplitGroup& split_group) {
    auto& vars = split_group.vars;
    PrimExpr extent = SubstByName(split_group.extent, this->sizes);
    auto ext_int = extent.as<IntImmNode>();
    if (!ext_int) {
      ext_int = SimplifyExpr(extent).as<IntImmNode>();
    }
    std::unordered_map<int, double> factors;
    if (ext_int) {
      if (ext_int->value == 1) {
        for (auto& var : vars) {
          this->var_decomp[var].clear();
        }
        return;
      }
      if (split_group.whole_div) {
        factors = Factorize(ext_int->value);
      } else {
        // No division requirement, no need to decompose.
        // Just put a size constraint on the variables.
        factors = {{2, std::log2((double)ext_int->value)}};
      }
    } else {
      // We don't really know the extent of the loop.
      ICHECK(!split_group.whole_div) << "Cannot require whole division with unknown extent";
      // Just set the variable bound to the default.
      factors = {{2, this->DEFAULT_UBOUND}};
    }
    for (auto [prime, power] : factors) {
      PrimExpr con = Integer(0);
      for (auto& varname : vars) {
        SizeVar vprime(varname + "_" + std::to_string(prime), DataType::Int(32), Span(), true);
        // sp_0_1 = 2**sp_0_1_2 * 3**sp_0_1_3 * 5**sp_0_1_5 * 7**sp_0_1_7
        this->var_decomp[varname][prime] = vprime;
        con += vprime;
      }
      this->factor_cons.push_back(less_equal(con, power));
    }
  }

  VarMapT sizes;
  std::vector<PrimExpr> factor_cons;

 public:
  DecompT var_decomp;
};

class BooleanCondRewriter : public ExprMutator {
 public:
  BooleanCondRewriter(const VarMapT& var_map = {}) : range_inf(Range(0, 5)), var_map(var_map) {}

  PrimExpr VisitExpr_(const LTNode* e) { return RangeStableDiff(VisitExpr(e->b), VisitExpr(e->a)); }
  PrimExpr VisitExpr_(const LENode* e) { return VisitExpr(LT(e->a, e->b)); }
  PrimExpr VisitExpr_(const GTNode* e) { return VisitExpr(LT(e->b, e->a)); }
  PrimExpr VisitExpr_(const GENode* e) { return VisitExpr(LT(e->b, e->a)); }
  PrimExpr VisitExpr_(const EQNode* e) {
    LOG_ERROR << "Equality condition unsupported";
    return PrimExpr();
  }
  PrimExpr VisitExpr_(const NENode* e) {
    LOG_ERROR << "Inequality condition unsupported";
    return PrimExpr();
  }
  PrimExpr VisitExpr_(const NotNode* e) { return -VisitExpr(e->a); }

 protected:
  PrimExpr RangeStableDiff(PrimExpr a, PrimExpr b) {
    if (PowLogBalanced(a - b)) {
      return a - b;
    }
    double amin, amax, bmin, bmax;
    ICHECK(ToConstRange(this->range_inf(a), amin, amax));
    ICHECK(ToConstRange(this->range_inf(b), bmin, bmax));
    bool reverse = amax <= 0 && bmax <= 0;
    if (reverse) {
      std::tie(a, b, amin, amax, bmin, bmax) = std::make_tuple(-b, -a, -bmax, -bmin, -amax, -amin);
    }
    if (amin == 0 || bmin == 0) {
      // HACK: try adding 1 to the range to make it non-zero
      a += 1, b += 1, amin += 1, amax += 1, bmin += 1, bmax += 1;
    }
    if (amin <= 0 || bmin <= 0) {
      std::cout << a << " " << amin << " " << b << " " << bmin << "\n";
      LOG_FATAL << "Cannot take diff of " << a << " and " << b << " to a range stable form";
      return PrimExpr();
    }
    auto diff = SimplifyExpr(log(a) - log(b), 20, 5000, true);
    ICHECK(PowLogBalanced(diff));
    return diff;
  }

  bool PowLogBalanced(const PrimExpr& e) {
    bool log_ = false, pow_ = false;
    PostOrderVisit(e, [&log_, &pow_](const ObjectRef& e) {
      if (auto call = e.as<CallNode>()) {
        if (auto op = call->op.as<OpNode>()) {
          if (op->name == "tir.log" || op->name == "tir.logk") {
            log_ = true;
          } else if (op->name == "tir.pow") {
            pow_ = true;
          }
        }
      }
    });
    return !pow_ || log_;
  }

  RangeInfer range_inf;
  const VarMapT& var_map;
};

class DiffableApprox : public BooleanCondRewriter {
 public:
  DiffableApprox(const VarMapT& var_map, const std::string& varname_prefix)
      : BooleanCondRewriter(var_map), prefix(varname_prefix) {}

  PrimExpr VisitExpr_(const LTNode* e) {
    if (!this->_simplified) {
      return RunOnSimplified(GetRef<PrimExpr>(e));
    }
    return InsertAsShorthand(sigmoid(RangeStableDiff(VisitExpr(e->b), VisitExpr(e->a))));
  }

  PrimExpr VisitExpr_(const EQNode* e) {
    if (!this->_simplified) {
      return RunOnSimplified(GetRef<PrimExpr>(e));
    }
    // Continue to VisitExpr the result in order to remove floordiv() and floormod()
    auto a = VisitExpr(e->a), b = VisitExpr(e->b);
    auto sigmoid_diff = TryConvertingToLT(a - b);
    if (sigmoid_diff.defined()) {
      return InsertAsShorthand(sigmoid_diff);
    }
    return InsertAsShorthand(hump(RangeStableDiff(a, b)));
  }

  // 1 - x below stands for `not x`
  PrimExpr VisitExpr_(const LENode* e) { return 1 - this->VisitExpr(LT(e->b, e->a)); }
  PrimExpr VisitExpr_(const GENode* e) { return 1 - this->VisitExpr(LT(e->a, e->b)); }
  PrimExpr VisitExpr_(const GTNode* e) { return this->VisitExpr(LT(e->b, e->a)); }
  PrimExpr VisitExpr_(const NENode* e) { return 1 - this->VisitExpr(EQ(e->a, e->b)); }

  PrimExpr VisitExpr_(const AndNode* e) { return VisitExpr(e->a) * VisitExpr(e->b); }
  PrimExpr VisitExpr_(const OrNode* e) {
    std::vector<PrimExpr> chained_ors;
    Collect(e, chained_ors);
    // Using de Morgan's law:
    PrimExpr ret = Integer(1);
    for (auto& expr : chained_ors) {
      ret *= 1 - VisitExpr(expr);
    }
    return 1 - ret;
  }
  PrimExpr VisitExpr_(const NotNode* e) { return 1 - VisitExpr(e->a); }

  PrimExpr VisitExpr_(const SelectNode* e) {
    auto cond = VisitExpr(e->condition), tv = VisitExpr(e->true_value),
         fv = VisitExpr(e->false_value);
    return cond * (tv - fv) + fv;
  }

  PrimExpr VisitExpr_(const FloorDivNode* e) {
    // Simply drop the floor() operator; it's not differentiable.
    return div(VisitExpr(e->a), VisitExpr(e->b));
  }
  PrimExpr VisitExpr_(const FloorModNode* e) {
    LOG_WARNING << "Mod operator is not differentiable and will be dropped.";
    return Integer(0);
  }

 private:
  template <typename T>
  void Collect(const T* e, std::vector<PrimExpr>& ret) {
    auto *lhs = e->a.template as<T>(), *rhs = e->b.template as<T>();
    if (lhs) {
      Collect(lhs, ret);
    } else {
      ret.push_back(e->a);
    }
    if (rhs) {
      Collect(rhs, ret);
    } else {
      ret.push_back(e->b);
    }
  }

  PrimExpr TryConvertingToLT(const PrimExpr& diff) {
    // approx(0 == x) ==
    //     1 - approx(0 < x) == sigmoid(-x)   if \forall x >= 0
    //     1 - approx(x < 0) == sigmoid(x)    if \forall x <= 0
    //     ...?                               otherwise
    double min_value, max_value;
    ICHECK(ToConstRange(this->range_inf(diff), min_value, max_value));
    if (min_value >= 0) {
      return sigmoid(RangeStableDiff(diff, Integer(0)));
    } else if (max_value <= 0) {
      return sigmoid(RangeStableDiff(Integer(0), diff));
    }
    return PrimExpr();
  }

  inline PrimExpr InsertAsShorthand(PrimExpr e) {
    String var_name = this->prefix + std::to_string(this->shorthands.size());
    this->shorthands[var_name] = e;
    return Var(var_name);
  }

  PrimExpr RunOnSimplified(const PrimExpr& e) {
    auto it = this->_memo.find(e);
    if (it != this->_memo.end()) {
      it->second.second++;
      return it->second.first;
    }
    this->_simplified = true;
    auto subst = SubstByName(e, this->var_map);
    size_t n_iters, n_nodes;
    size_t nops = CountOps(subst);
    if (nops > 50) {
      n_iters = 30, n_nodes = 20000;
    } else if (nops > 20) {
      n_iters = 20, n_nodes = 5000;
    } else {
      n_iters = 10, n_nodes = 2000;
    }
    PrimExpr ret = this->VisitExpr(SimplifyExpr(subst, n_iters, n_nodes, true));
    this->_simplified = false;
    this->_memo[e] = {ret, 1};
    return ret;
  }

 public:
  VarMapT shorthands;

 private:
  std::string prefix;
  bool _simplified = false;
  std::unordered_map<PrimExpr, std::pair<PrimExpr, size_t>, StructuralHash, StructuralEqual> _memo;
};

class FeaturePackPyNode : public Object {
 public:
  Array<PrimExpr> features, other_cons;
  Array<LinearExpr> lin_neg_cons;
  Map<String, PrimExpr> var_map;
  Array<String> all_variables;
  Map<String, Map<Integer, SizeVar>> var_decomp;
  IntImm n_feats;

  void VisitAttrs(AttrVisitor* v) {
    v->Visit("features", &features);
    v->Visit("other_cons", &other_cons);
    v->Visit("lin_neg_cons", &lin_neg_cons);
    v->Visit("var_map", &var_map);
    v->Visit("all_variables", &all_variables);
    v->Visit("var_decomp", &var_decomp);
    v->Visit("n_feats", &n_feats);
  }

  static constexpr const char* _type_key = "ansor.FeaturePackPy";
  TVM_DECLARE_FINAL_OBJECT_INFO(FeaturePackPyNode, Object);
};

TVM_REGISTER_NODE_TYPE(FeaturePackPyNode);

class FeaturePackPy : public ObjectRef {
 public:
  TVM_DEFINE_OBJECT_REF_METHODS(FeaturePackPy, ObjectRef, FeaturePackPyNode);
};

class FeaturePack {
 public:
  void RunExpTransform(const VarMapT& sizes, bool factorize) {
    ExpTransform etr =
        factorize ? ExpTransform(sizes, this->split_groups) : ExpTransform(sizes, {});
    // 1. List all vars in `features` and `var_map` (excluding shorthands already in `var_map` and
    // vars in `sizes`), and create exp decomposition for them. This is then inserted into
    // `var_map`.
    for (auto& expr : this->features) {
      etr.RunOnExpr(expr, this->var_map);
    }
    for (auto& [_, expr] : this->var_map.get_exprs()) {
      etr.RunOnExpr(expr, this->var_map);
    }
    this->var_map.prepend(etr.GetVarMap());
    // 2. Substitute `sizes` into `var_map` first and simplify.
    // Then do a rolling substitution and simplification from the front to the back of `var_map`,
    // popping any variable that is reduced to constant in this process.
    // This may help a lot in the following simplification process.
    auto& vexprs = this->var_map.get_exprs();
    support::parallel_for(0, vexprs.size(), [&vexprs, &sizes](int i) {
      vexprs[i].second = SubAndSimplify(vexprs[i].second, sizes, false);
    });
    VarMapT simple_vars;
    for (auto& [vname, expr] : vexprs) {
      expr = SubAndSimplify(expr, simple_vars, false, 20, 1000);
      if (expr->IsInstance<IntImmNode>() || expr->IsInstance<FloatImmNode>()) {
        simple_vars[vname] = expr;
      }
    }
    VarMapT subst = sizes;
    subst.merge(simple_vars);
    // 3. Substitute `subst` into `features` and simplify.
    // Note that `subst` doesn't fully expand shorthand variables in `features`, thus keeping them
    // short.
    support::parallel_for(0, features.size(), [this, &subst](int i) {
      features[i] = SubAndSimplify(this->features[i], subst, true);
    });
    // 4. Add constraints incurred by the exp decomposition into our constraints,
    // and simplify constraints with maximal substitution.
    std::vector<PrimExpr> constraints_;
    std::tie(this->free_vars, constraints_) = etr.GetVarsAndConstraints();
    constraints_.insert(constraints_.end(), this->constraints.begin(), this->constraints.end());
    this->constraints.clear();
    subst.merge(var_map.expand_into_subst_map());
    for (auto& expr : constraints_) {
      PrimExpr expr_ = SubAndSimplify(expr, subst, true);
      if (!expr_->IsInstance<IntImmNode>()) {
        this->constraints.push_back(expr_);
      }
      // Otherwise fully simplified to True.
    }
    // 5. Set var decomposition (used on Python side), drop split group.
    this->var_decomp = std::move(etr.var_decomp);
    this->split_groups.clear();
  }

  void RunDiffTransform(size_t n_threads) {
    // 1. Separate constaints into linear and non-linear ones.
    auto constraints = std::move(this->constraints);
    this->constraints.clear();
    for (auto& expr : constraints) {
      // Add negative sign to turn constraints into negative-better form.
      PrimExpr diff = SimplifyExpr(-BooleanCondRewriter()(expr), 30, 10000, true);
      auto lin_diff = LinExprExtractor()(diff);
      if (lin_diff.defined()) {
        this->linear_cons.push_back(lin_diff);
      } else {
        this->constraints.push_back(diff);
      }
    }
    // 2. Run differentiability approximations on features and left-over constraints.
    // Use N threads for a balance between speed and reuse of memo table.
    std::vector<DiffableApprox> das;
    auto subst = var_map.expand_into_subst_map();
    for (size_t i = 0; i < n_threads; ++i) {
      das.emplace_back(subst, "da_" + std::to_string(i) + "_");
    }
    auto worklist = MakeWorkList();
    support::parallel_for_dynamic(0, worklist.size(), n_threads,
                                  [&worklist, &das](int thread_id, int task_id) {
                                    *worklist[task_id] = das[thread_id](*worklist[task_id]);
                                  });
    // 3. Regroup variables created in DA process
    VarMapT eq_var_subst;
    for (auto& da : das) {
      for (auto& [vname, expr] : da.shorthands) {
        auto new_name = "da_" + std::to_string(eq_var_subst.size());
        eq_var_subst[vname] = this->var_map.try_push_back(new_name, expr);
      }
    }
    for (auto* expr : MakeWorkList()) {
      *expr = SubstByName(*expr, eq_var_subst);
    }
  }

  FeaturePackPy IntoPythonFeaturePack() const {
    auto node = make_object<FeaturePackPyNode>();
    node->features = this->features;
    node->other_cons = this->constraints;
    node->lin_neg_cons = this->linear_cons;
    for (auto& [k, v] : this->var_map.get_exprs()) {
      node->var_map.Set(k, v);
    }
    for (auto& vname : this->free_vars) {
      node->all_variables.push_back(vname);
    }
    for (auto& [k1, vs] : this->var_decomp) {
      Map<Integer, SizeVar> m;
      for (auto& [k2, v] : vs) {
        m.Set(k2, v);
      }
      node->var_decomp.Set(k1, m);
    }
    node->n_feats = Integer(this->feat_names.size());
    return FeaturePackPy(node);
  }

  static std::optional<FeaturePack> LoadFromJsonReader(dmlc::JSONReader& reader) {
    bool is_defined;
    reader.BeginArray();
    ICHECK(reader.NextArrayItem());
    reader.Read(&is_defined);
    if (!is_defined) {
      return std::nullopt;
    }
    FeaturePack fp;
    ICHECK(reader.NextArrayItem());
    fp.ReadFeatsMap(reader);
    ICHECK(reader.NextArrayItem());
    reader.Read(&fp.constraints);
    ICHECK(reader.NextArrayItem());
    reader.Read(&fp.linear_cons);
    ICHECK(reader.NextArrayItem());
    reader.Read(&fp.var_map);
    ICHECK(reader.NextArrayItem());
    reader.Read(&fp.free_vars);
    ICHECK(reader.NextArrayItem());
    reader.Read(&fp.split_groups);
    ICHECK(!reader.NextArrayItem());
    return fp;
  }

  static void SaveAsJson(const String& filepath, const std::optional<FeaturePack>& fp) {
    std::ofstream fout(filepath);
    dmlc::JSONWriter writer(&fout);
    writer.BeginArray(true);
    writer.WriteArrayItem((bool)fp);
    if (fp) {
      writer.WriteArraySeperator();
      fp->WriteFeatsMap(writer);
      writer.WriteArrayItem(fp->constraints);
      writer.WriteArrayItem(fp->linear_cons);
      writer.WriteArrayItem(fp->var_map);
      writer.WriteArrayItem(fp->free_vars);
      writer.WriteArrayItem(fp->split_groups);
    }
    writer.EndArray();
  }

 private:
  std::vector<PrimExpr*> MakeWorkList() {
    std::vector<PrimExpr*> worklist;
    for (auto& expr : this->features) {
      worklist.push_back(&expr);
    }
    for (auto& [_, expr] : this->var_map.get_exprs()) {
      worklist.push_back(&expr);
    }
    return worklist;
  }

  void WriteFeatsMap(dmlc::JSONWriter& writer) const {
    size_t n_feats = this->feat_names.size();
    ICHECK(this->features.size() % n_feats == 0) << "Features and names mismatch";
    size_t n_bufs = this->features.size() / n_feats;
    writer.BeginArray();
    for (size_t i = 0; i < n_bufs; ++i) {
      writer.WriteArraySeperator();
      writer.BeginObject();
      for (size_t j = 0; j < n_feats; ++j) {
        writer.WriteObjectKeyValue(this->feat_names[j], this->features[i * n_feats + j]);
      }
      writer.EndObject();
    }
    writer.EndArray();
  }

  void ReadFeatsMap(dmlc::JSONReader& reader) {
    this->feat_names.clear();
    this->features.clear();
    reader.BeginArray();
    auto& fnames = this->feat_names;
    while (reader.NextArrayItem()) {
      reader.BeginObject();
      std::string fname;
      PrimExpr feat;
      for (size_t idx = 0; reader.NextObjectItem(&fname); ++idx) {
        if (fnames.size() > idx) {
          ICHECK(fname == fnames[idx]) << "Features and names mismatch";
        } else {
          fnames.push_back(fname);
        }
        reader.Read<PrimExpr>(&feat);
        this->features.push_back(feat);
      }
    }
  }

 public:
  std::vector<PrimExpr> features, constraints;
  std::vector<std::string> feat_names;
  OrderedVarMap var_map;
  std::vector<SplitGroup> split_groups;
  std::vector<LinearExpr> linear_cons{};
  std::vector<std::string> free_vars{};
  DecompT var_decomp;
};

TVM_REGISTER_GLOBAL("auto_scheduler.GetFeaturePack")
    .set_body_typed([](Stmt stmt, VarContext context, HardwareParams hw_params,
                       Map<String, PrimExpr> sizes, size_t cache_line_size, size_t max_n_bufs,
                       bool factorize, String save_load_path) {
      std::ifstream fin(save_load_path);
      FeaturePack fp;
      if (fin.is_open()) {
        dmlc::JSONReader reader(&fin);
        auto fp_opt = FeaturePack::LoadFromJsonReader(reader);
        if (!fp_opt) {
          LOG_WARNING << "File " << save_load_path << " notes previous feature extraction failure";
          return FeaturePackPy();  // None
        }
        fp = fp_opt.value();
      } else {
        LOG_INFO << "Extracting features to save to " << save_load_path;
        auto context_p = context.CopyOnWrite();
        std::vector<PrimExpr> features;
        try {
          features = GetPerStoreFeatureExpr(stmt, *context_p, cache_line_size, max_n_bufs);
        } catch (const std::exception& e) {
          LOG_WARNING << "Feature extraction failed: " << e.what();
          FeaturePack::SaveAsJson(save_load_path, std::nullopt);
          return FeaturePackPy();  // None
        }
        support::parallel_for(0, features.size(),
                              [&](int i) { features[i] = SimplifyExpr(features[i]); });
        auto constraints = GetConstraints(stmt, hw_params);
        for (auto& expr : constraints) {
          expr = SimplifyExpr(expr);
        }
        fp = FeaturePack{std::move(features), std::move(constraints),
                         GetPerStoreFeatureName(max_n_bufs), context_p->var_to_expr,
                         context_p->split_groups};
        FeaturePack::SaveAsJson(save_load_path, fp);
      }
      VarMapT sizes_(sizes.begin(), sizes.end());
      fp.RunExpTransform(sizes_, factorize);
      fp.RunDiffTransform(16);
      FeaturePack::SaveAsJson(save_load_path + ".json", fp);
      return fp.IntoPythonFeaturePack();
    });

TVM_REGISTER_GLOBAL("auto_scheduler.LinearExprAsPrimExpr").set_body_typed([](LinearExpr e) {
  return e.ToPrimExpr();
});

}  // namespace auto_scheduler
}  // namespace tvm

namespace dmlc {
namespace json {
template <>
struct Handler<::tvm::auto_scheduler::LinearExpr> {
  inline static void Write(JSONWriter* writer, const tvm::auto_scheduler::LinearExpr& e) {
    writer->BeginArray();
    writer->WriteArrayItem(e->constant->value);
    std::unordered_map<std::string, double> name2coef;
    for (auto& [var, coef] : e->lin_terms) {
      name2coef[var->name_hint] = coef->value;
    }
    writer->WriteArrayItem(name2coef);
    writer->EndArray();
  }
  inline static void Read(JSONReader* reader, tvm::auto_scheduler::LinearExpr* e) {
    reader->BeginArray();
    ICHECK(reader->NextArrayItem());
    double constant;
    reader->Read(&constant);
    ICHECK(reader->NextArrayItem());
    std::unordered_map<std::string, double> name2coef;
    reader->Read(&name2coef);
    ICHECK(!reader->NextArrayItem());
    *e = tvm::auto_scheduler::LinearExpr(constant, name2coef);
  }
};
}  // namespace json
}  // namespace dmlc
