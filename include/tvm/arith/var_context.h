#ifndef TVM_ARITH_VAR_CONTEXT_H_
#define TVM_ARITH_VAR_CONTEXT_H_

#include <tvm/arith/egg_simpl.h>
#include <tvm/node/reflection.h>
#include <tvm/te/schedule.h>
#include <tvm/tir/expr.h>

namespace tvm {
namespace arith {

inline bool ExprIsConst(const PrimExpr& e) {
  bool is_const = true;
  tir::PostOrderVisit(e, [&is_const](const ObjectRef& node) {
    if (node->IsInstance<VarNode>() && !node->IsInstance<tir::SizeVarNode>()) {
      is_const = false;
    }
  });
  return is_const;
}

using VarMapT = std::unordered_map<std::string, PrimExpr>;

class OrderedVarMap {
  using ContainerT = std::vector<std::pair<std::string, PrimExpr>>;

 public:
  OrderedVarMap() = default;

  tir::SizeVar push_back(const std::string& vname, PrimExpr expr) {
    this->exprs.emplace_back(vname, expr);
    return push_aux(vname, expr);
  }

  tir::SizeVar try_push_back(const std::string& vname, const PrimExpr& expr) {
    auto it = this->expr2var.find(expr);
    if (it != this->expr2var.end()) {
      return it->second;
    }
    return this->push_back(vname, expr);
  }

  void prepend(const VarMapT& vmap) {
    ContainerT new_exprs;
    for (auto& [vname, expr] : vmap) {
      ICHECK(!this->contains(vname));
      new_exprs.emplace_back(vname, expr);
      push_aux(vname, expr);
    }
    this->exprs.insert(this->exprs.begin(), new_exprs.begin(), new_exprs.end());
  }

  VarMapT expand_into_subst_map() const {
    VarMapT vmap;
    for (const auto& [vname, expr] : this->exprs) {
      vmap.emplace(vname, tir::SubstByName(expr, vmap));
    }
    return vmap;
  }

  bool contains(const std::string& vname) const {
    return this->vnames.find(vname) != this->vnames.end();
  }
  size_t size() const { return this->exprs.size(); }
  const ContainerT& get_exprs() const { return this->exprs; }
  ContainerT& get_exprs() { return this->exprs; }

 private:
  tir::SizeVar push_aux(const std::string& vname, const PrimExpr& expr) {
    this->vnames.insert(vname);
    tir::SizeVar var(vname, expr->dtype);
    this->expr2var.emplace(expr, var);
    return var;
  }

  ContainerT exprs;
  std::unordered_set<std::string> vnames;
  std::unordered_map<PrimExpr, tir::SizeVar, StructuralHash, StructuralEqual> expr2var;
};

inline std::ostream& operator<<(std::ostream& os, const OrderedVarMap& vmap) {
  auto vname_expr = vmap.get_exprs();
  os << "{";
  for (size_t i = 0; i < vname_expr.size(); i++) {
    const auto& [vname, expr] = vname_expr[i];
    if (i != 0) {
      os << ",\n";
    }
    os << vname << ": " << expr;
  }
  os << "}";
  return os;
}

class SplitGroup {
 public:
  SplitGroup(PrimExpr extent, Array<String> vars, bool whole_div)
      : extent(std::move(extent)), vars(std::move(vars)), whole_div(Bool(whole_div)) {}

  SplitGroup() = default;

  PrimExpr extent;
  Array<String> vars;
  Bool whole_div{false};
};

class VarContextNode : public Object {
 public:
  std::vector<SplitGroup> split_groups{};
  OrderedVarMap var_to_expr{};
  std::unordered_multimap<PrimExpr, PrimExpr, StructuralHash, StructuralEqual> _div_map{};
  size_t split_counter{};

  void VisitAttrs(tvm::AttrVisitor* v) {}

  std::pair<PrimExpr, PrimExpr> GetSplitSizes(PrimExpr extent, PrimExpr factor,
                                              bool no_tighten_factor);
  PrimExpr AttemptShorten(PrimExpr expr);

  // Insert and return the extent of a for loop as a new variable.
  tir::SizeVar MakeAndInsertVar(PrimExpr expr, bool use_in_subst);

 private:
  std::pair<PrimExpr, PrimExpr> SymbolicDiv(PrimExpr numer, PrimExpr denom, bool no_tighten_factor);

 public:
  static constexpr const char* _type_key = "arith.VarContext";
  TVM_DECLARE_FINAL_OBJECT_INFO(VarContextNode, Object);
};

class VarContext : public ObjectRef {
 public:
  static VarContext MakeContext() {
    auto node = make_object<VarContextNode>();
    return VarContext(node);
  }

  Array<tir::SizeVar> GetSplitVars(PrimExpr extent, size_t n_splits, bool whole_div);

  TVM_DEFINE_OBJECT_REF_METHODS(VarContext, ObjectRef, VarContextNode);
  TVM_DEFINE_OBJECT_REF_COW_METHOD(VarContextNode);
};
}  // namespace arith
}  // namespace tvm

namespace dmlc {
namespace json {

template <typename T>
struct Handler<tvm::Array<T>> {
  inline static void Write(dmlc::JSONWriter* writer, const tvm::Array<T>& array) {
    writer->BeginArray();
    for (const auto& item : array) {
      writer->WriteArrayItem(item);
    }
    writer->EndArray();
  }

  inline static void Read(dmlc::JSONReader* reader, tvm::Array<T>* array) {
    array->clear();
    reader->BeginArray();
    while (reader->NextArrayItem()) {
      T item;
      reader->Read(&item);
      array->push_back(item);
    }
  }
};

template <typename V>
struct Handler<tvm::Map<tvm::String, V>> {
  inline static void Write(dmlc::JSONWriter* writer, const tvm::Map<tvm::String, V>& map) {
    writer->BeginObject();
    for (const auto& [k, v] : map) {
      writer->WriteObjectKeyValue(k, v);
    }
    writer->EndObject();
  }

  inline static void Read(dmlc::JSONReader* reader, tvm::Map<tvm::String, V>* map) {
    map->clear();
    reader->BeginObject();
    std::string k;
    V v;
    while (reader->NextObjectItem(&k)) {
      reader->Read(&v);
      map->Set(k, v);
    }
  }
};

template <>
struct Handler<tvm::arith::SplitGroup> {
  inline static void Write(dmlc::JSONWriter* writer, const tvm::arith::SplitGroup& group) {
    writer->BeginArray();
    writer->WriteArrayItem(group.extent);
    writer->WriteArrayItem(group.vars);
    writer->WriteArrayItem(group.whole_div->value);
    writer->EndArray();
  }

  inline static void Read(dmlc::JSONReader* reader, tvm::arith::SplitGroup* group) {
    tvm::PrimExpr extent;
    tvm::Array<tvm::String> vars;
    bool whole_div;
    reader->BeginArray();
    ICHECK(reader->NextArrayItem());
    reader->Read(&extent);
    ICHECK(reader->NextArrayItem());
    reader->Read(&vars);
    ICHECK(reader->NextArrayItem());
    reader->Read<bool>(&whole_div);
    ICHECK(!reader->NextArrayItem());
    *group = tvm::arith::SplitGroup(std::move(extent), std::move(vars), tvm::Bool(whole_div));
  }
};

template <>
struct Handler<tvm::arith::OrderedVarMap> {
  inline static void Write(dmlc::JSONWriter* writer, const tvm::arith::OrderedVarMap& vmap) {
    writer->BeginObject();
    for (const auto& [k, v] : vmap.get_exprs()) {
      writer->WriteObjectKeyValue(k, v);
    }
    writer->EndObject();
  }

  inline static void Read(dmlc::JSONReader* reader, tvm::arith::OrderedVarMap* group) {
    reader->BeginObject();
    std::string vname;
    tvm::PrimExpr expr;
    while (reader->NextObjectItem(&vname)) {
      reader->Read(&expr);
      group->push_back(vname, expr);
    }
  }
};
}  // namespace json
}  // namespace dmlc
#endif
